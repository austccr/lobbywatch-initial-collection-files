# Lobbywatch Initial Collection Files

The files of the original Lobbywatch collection, which was collated in Google Sheets. This repo contains the raw CSV files exported from Google sheets, and normalised CSV files for importing into the Lobbywatch application.

Find raw files in the `raw` directory.

Find normalised files in the `normalised` directory.

Files are named for the date they were exported and the lobby group they relate to.

## Normalisation process

### 1. Remove extra columns

Delete all columns expect these 6:

1. `Date`
2. `Link`
3. `Source`
4. `Summary`
5. `Name`
6. `Classification`

If there are missing columns, add them, and **arrange them in the order above**.

### 2. Rename columns

Rename the columns as follows, all to lowercase:

1. `Date` -> `date`
2. `Link` -> `url`
3. `Source` -> `source`
4. `Summary` -> `summary`
5. `Name` -> `featured_person`
6. `Classification` -> `topic`

### 3. Normalise empty cells

If a row has no value for a cell, it make it empty.
Remove instances of `-` that represent an empty cell.

### 4. Format dates

Format dates in the `date` column as `9-Nov-2018` (without zero-padded days).

### 5. Remove duplicate entries

We should only be capturing each item (e.g. URL) once. In other words, there
should be no two entries with the same value in the `url` column.

A good way to track these down is to open your file in some spreadsheet
software, and make a pivot table for the whole sheet. Make the rows `url`, and
the value/data field put the Count of each `url`. You can then sort the table by
the count of each `url`, and find the duplicates grouped at the top or bottom.
